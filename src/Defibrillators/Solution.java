package Defibrillators;
import java.util.*;
import java.io.*;
import java.math.*;
import java.util.Arrays;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    static String delimiter = ";";

    public static void main(String args[]) {

        Solution solution = new Solution();

        Scanner in = new Scanner(System.in);
        String DEFIB = null;
        String LON = in.next();
        String LAT = in.next();

        double closestDist = Double.MAX_VALUE;

        double lonB = 0;
        double latB = 0;

        int closestNum = 0;
        String closestName = "";
        int N = in.nextInt();

        if (in.hasNextLine()) {
            in.nextLine();
        }


        for (int i = 0; i < N; i++) {
            DEFIB = in.nextLine();
            //print all data about defibrillator
            System.err.println(DEFIB);

            String[] subStr = DEFIB.split(delimiter);

            //find and convert longitude and latitiude
            for (int j = 0; j < subStr.length; j++) {
                if (j == 4) {
                    lonB = Double.valueOf(subStr[j].replace(",", "."));
                    System.err.println("LonB: " + lonB);
                }

                if (j == 5) {
                    latB = Double.valueOf(subStr[j].replace(",", "."));
                    System.err.println("LatB: " + latB);
                }

                double lastDistance = solution.getDistanceBetween(solution.convertToDouble(LON), solution.convertToDouble(LAT), lonB, latB);
                if (lastDistance < closestDist) {
                    closestDist = lastDistance;
                    closestName  = subStr[1];
                }

            }
        }

        System.out.println(closestName);

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

    }

    public double getDistanceBetween(double lonA, double latA, double lonB, double latB) {
        double x = (lonB - lonA) * Math.cos((latA + latB)/2);
        double y = latB - latA;

        double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) * 6371;
        // System.err.println("lonB = " + lonA);

        return distance;
    }

    //string with "," replace with "." and convert to double
    public double convertToDouble(String str) {
        String replaced = str.replace(",",".");
        double converted = Double.valueOf(replaced);
        return converted;
    }
}